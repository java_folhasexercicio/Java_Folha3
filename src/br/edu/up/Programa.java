package br.edu.up;

import java.util.Scanner;
import javax.naming.directory.AttributeModificationException;
import br.edu.up.Exercicios.*;

public class Programa {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int opcaoMenu = 0;

        System.out.println("Informe o exercício que quer executar, entre 1 e 26.");
        opcaoMenu = scanner.nextInt();
        scanner.nextLine();

        switch (opcaoMenu) {
            case 1:
                Ex1 ex1 = new Ex1();
                ex1.Executar();
                break;
            case 2:
                Ex2 ex2 = new Ex2();
                ex2.Executar();
                break;
            case 3:
                Ex3 ex3 = new Ex3();
                ex3.Executar();
                break;
            case 4:
                Ex4 ex4 = new Ex4();
                ex4.Executar();
                break;
            case 5:
                Ex5 ex5 = new Ex5();
                ex5.Executar();
                break;
            case 6:
                Ex6 ex6 = new Ex6();
                ex6.Executar();
                break;
            case 7:
                Ex7 ex7 = new Ex7();
                ex7.Executar();
                break;
            case 8:
                Ex8 ex8 = new Ex8();
                ex8.Executar();
                break;
            case 9:
                Ex9 ex9 = new Ex9();
                ex9.Executar();
                break;
            case 10:
                Ex10 ex10 = new Ex10();
                ex10.Executar();
                break;
            case 11:
                Ex11 ex11 = new Ex11();
                ex11.Executar();
                break;
            case 12:
                Ex12 ex12 = new Ex12();
                ex12.Executar();
                break;
            case 13:
                Ex13 ex13 = new Ex13();
                ex13.Executar();
                break;
            case 14:
                Ex14 ex14 = new Ex14();
                ex14.Executar();
                break;
            case 15:
                Ex15 ex15 = new Ex15();
                ex15.Executar();
                break;
            case 16:
                Ex16 ex16 = new Ex16();
                ex16.Executar();
                break;
            case 17:
                Ex17 ex17 = new Ex17();
                ex17.Executar();
                break;
            case 18:
                Ex18 ex18 = new Ex18();
                ex18.Executar();
                break;
            case 19:
                Ex19 ex19 = new Ex19();
                ex19.Executar();
                break;
            case 20:
                Ex20 ex20 = new Ex20();
                ex20.Executar();
                break;
            case 21:
                Ex21 ex21 = new Ex21();
                ex21.Executar();
                break;
            case 22:
                Ex22 ex22 = new Ex22();
                ex22.Executar();
                break;
            // case 23:
            // Ex23 ex23 = new Ex23();
            // ex23.Executar();
            // break;
            // case 24:
            // Ex24 ex24 = new Ex24();
            // ex24.Executar();
            // break;
            // case 25:
            // Ex25 ex25 = new Ex25();
            // ex25.Executar();
            // break;
            // case 26:
            // Ex26 ex26 = new Ex26();
            // ex26.Executar();
            // break;
            // case 27:
            // Ex27 ex27 = new Ex27();
            // ex27.Executar();
            // break;
            default:
                System.out.println("Opção inválida.");
        }

        scanner.close();
    }

}
