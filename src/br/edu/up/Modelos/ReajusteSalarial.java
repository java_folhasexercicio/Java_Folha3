package br.edu.up.Modelos;

public class ReajusteSalarial {

    private String nomeFuncionario;
    private double salarioAtual;
    private double salarioMinimo;

    public ReajusteSalarial(String nomeFuncionario, double salarioAtual, double salarioMinimo) {
        this.nomeFuncionario = nomeFuncionario;
        this.salarioAtual = salarioAtual;
        this.salarioMinimo = salarioMinimo;
    }

    public double calculaIndiceReajuste() {
        return (salarioMinimo - this.salarioAtual) / this.salarioAtual;
    }

    public double calculaReajuste() {
        return this.salarioAtual * calculaIndiceReajuste();
    }

    public double calculaNovoSalario() {
        return this.salarioAtual + calculaReajuste();
    }

    public void imprimeResultados() {
        System.out.println("Funcionário: " + this.nomeFuncionario);
        System.out.println("Salário atual: R$ " + String.format("%.2f", this.salarioAtual));
        System.out.println("Salário mínimo: R$ " + String.format("%.2f", this.salarioMinimo));

        System.out.println("\n**Cálculo do reajuste:**");
        System.out.println("Índice de reajuste: " + String.format("%.2f%%", calculaIndiceReajuste() * 100));
        System.out.println("Reajuste: R$ " + String.format("%.2f", calculaReajuste()));

        System.out.println("\n**Novo salário:**");
        System.out.println("Novo salário: R$ " + String.format("%.2f", calculaNovoSalario()));
    }
}