package br.edu.up.Modelos;

public class Nadador {
    public int Idade;

    public String classificarCategoria() {
        if (Idade >= 5 && Idade <= 7) {
            return "Infantil A";
        } else if (Idade >= 8 && Idade <= 10) {
            return "Infantil B";
        } else if (Idade >= 11 && Idade <= 13) {
            return "Juvenil A";
        } else if (Idade >= 14 && Idade <= 17) {
            return "Juvenil B";
        } else if (Idade >= 18 && Idade <= 25) {
            return "Sênior";
        } else {
            return "Idade fora da faixa etária";
        }
    }

}
