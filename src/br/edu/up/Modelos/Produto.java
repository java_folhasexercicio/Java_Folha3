//Faça um programa que receba o preço de custo de um produto 
//e mostre o valor de venda. Sabe-se que o preço de custo
 //receberá um acréscimo de acordo com um percentual informado pelousuário

package br.edu.up.Modelos;

public class Produto {
    public double PrecoCusto;
    public double ValorVenda;
    public double Percentual;

    public Produto(double precoCusto, double percentual) {
        this.PrecoCusto = precoCusto;
        this.Percentual = percentual;
    }

    public double calcularValorVenda() {
        double acrescimo = PrecoCusto * (Percentual / 100.0);
        ValorVenda = PrecoCusto + acrescimo;
        return ValorVenda;
    }
}
