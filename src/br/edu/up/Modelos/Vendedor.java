package br.edu.up.Modelos;
public class Vendedor{

    public double salarioFixo;
    public double totalVendas;
    public String Nome;

    public Vendedor(String nome, double salarioFixo, double totalVendas) 
    {
        this.Nome = nome;

        this.salarioFixo = salarioFixo;

        this.totalVendas = totalVendas;
    }

    public double calculaSalario() {
        double comissao = totalVendas * 0.15;

        double salarioFinal = salarioFixo + comissao;

        return salarioFinal;
    }
    }
