package br.edu.up.Modelos;

public class Aluno {

    public int Matricula;
    public String Nome;
    public double Media;
    public String Status;

    public Aluno(String nome, double nota1, double nota2, double nota3) {

        this.Nome = nome;

        this.Media = (nota1 + nota2 + nota3) / 3;

        if (Media >= 7) {
            this.Status = "Aprovado";
        } else if (Media <= 5) {
            this.Status = "Reprovado";
        } else {
            this.Status = "Recuperação";
        }
    }
}
