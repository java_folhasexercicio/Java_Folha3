package br.edu.up.Modelos;

public class Automovel {

    public double DistanciaPercorrida;
    public double CombustivelGasto;
    public double Consumo;

    public double CalculaConsumo() {
        return this.Consumo = this.DistanciaPercorrida / this.CombustivelGasto;
    }

}
