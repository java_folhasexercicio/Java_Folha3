package br.edu.up.Modelos;

public class Professor {
    private String nome;
    private int nivel;
    private int horasAula;

    public Professor(String nome, int nivel, int horasAula) {
        this.nome = nome;
        this.nivel = nivel;
        this.horasAula = horasAula;
    }

    public double calcularValorHoraAula() {
        double valorHoraAula;
        switch (nivel) {
            case 1:
                valorHoraAula = 12.00;
                break;
            case 2:
                valorHoraAula = 17.00;
                break;
            case 3:
                valorHoraAula = 25.00;
                break;
            default:
                valorHoraAula = 0.00;
                System.out.println("Nível inválido. Valor por hora/aula definido como R$0,00.");
        }
        return valorHoraAula;
    }

    public double calcularSalarioBruto() {
        return calcularValorHoraAula() * horasAula;
    }

    public void imprimirResumoSalario() {
        System.out.println("\nResumo do Salário do Professor(a) " + nome + ":");
        System.out.printf("Nível: %d\n", nivel);
        System.out.printf("Horas/Aula Trabalhadas: %d\n", horasAula);
        System.out.printf("Valor por Hora/Aula: R$ %.2f\n", calcularValorHoraAula());
        System.out.printf("Salário Bruto: R$ %.2f\n", calcularSalarioBruto());
    }
}
