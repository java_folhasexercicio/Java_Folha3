//Ler 80 números e ao final informar quantos número(s) 
//est(á)ão no intervalo entre 10 (inclusive)e 150 (inclusive)

package br.edu.up.Modelos;

public class Intervalo {
        private int numero;
        private boolean dentroDoIntervalo;
    
        public Intervalo(int numero) {
            this.numero = numero;
            this.dentroDoIntervalo = verificarIntervalo(numero);
        }
    
        public boolean verificarIntervalo(int numero) {
            return (numero >= 10 && numero <= 150);
        }
    
        public boolean estaDentroDoIntervalo() {
            return dentroDoIntervalo;
        }
    
    }