package br.edu.up.Modelos;

public class CalculadoraNota {

    public double NotaLaboratorio;
    public double NotaAvSemestral;
    public double ExameFinal;
    public String ClassificacaoFinal;

    public double CalculaMedia() {

        double pesoLaboratorio = 2.0;
        double pesoAvaliacaoSemestral = 3.0;
        double pesoExameFinal = 5.0;

        var notaFinal = (NotaLaboratorio * pesoLaboratorio +
                NotaAvSemestral * pesoAvaliacaoSemestral +
                ExameFinal * pesoExameFinal)
                / (pesoLaboratorio + pesoAvaliacaoSemestral + pesoExameFinal);

        if (notaFinal > 8 && notaFinal < 10) {
            this.ClassificacaoFinal = "A";
        } else if (notaFinal > 7 && notaFinal < 8) {
            this.ClassificacaoFinal = "B";
        } else if (notaFinal > 6 && notaFinal < 7) {
            this.ClassificacaoFinal = "C";
        } else if (notaFinal > 5 && notaFinal < 6) {
            this.ClassificacaoFinal = "D";
        } else if (notaFinal > 0 && notaFinal < 5) {
            this.ClassificacaoFinal = "R";
        }

        return notaFinal;

    }

}
