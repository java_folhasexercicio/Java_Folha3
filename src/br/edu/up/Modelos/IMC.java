package br.edu.up.Modelos;

public class IMC {
    public double Altura;
    public int Idade;
    public double PesoIdeal;

    public void CalculaImcHomem() {

        if (this.Altura > 1.70) {

            if (this.Idade <= 20) {

                this.PesoIdeal = (72 * this.Altura) - 58;
            } else if (this.Idade >= 21 && this.Idade <= 39) {
                this.PesoIdeal = (72 * this.Altura) - 53;
            } else if (this.Idade >= 40) {
                this.PesoIdeal = (72 * this.Altura) - 45;
            }
        } else {
            if (this.Idade <= 40) {
                this.PesoIdeal = (72 * this.Altura) - 50;
            } else {
                this.PesoIdeal = (72 * this.Altura) - 58;
            }
        }

    }

    public void CalculaImcMulher() {

        if (this.Altura <= 1.50) {

            if (this.Idade >= 35) {

                this.PesoIdeal = (62.1 * this.Altura) - 45;
            } else {
                this.PesoIdeal = (62.1 * this.Altura) - 49;
            }
        } else {
            this.PesoIdeal = (62.1 * this.Altura) - 44.7;
        }

    }
}
