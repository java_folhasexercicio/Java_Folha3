package br.edu.up.Modelos;

public class ContaLuz {

    private String nomeCliente;
    private int tipoCliente;
    private double kWhConsumidos;
    private double valorTotal;

    public ContaLuz(String nomeCliente, int tipoCliente, double kWhConsumidos) {
        this.nomeCliente = nomeCliente;
        this.tipoCliente = tipoCliente;
        this.kWhConsumidos = kWhConsumidos;
        this.calcularValorTotal();
    }

    private void calcularValorTotal() {
        double valorKWh = 0.0;
        switch (tipoCliente) {
            case 1:
                valorKWh = 0.60;
                break;
            case 2:
                valorKWh = 0.48;
                break;
            case 3:
                valorKWh = 1.29;
                break;
        }

        valorTotal = valorKWh * kWhConsumidos;
    }

    public void imprimirResumo() {
        System.out.println("\nResumo da Conta de Luz do Cliente " + nomeCliente + ":");
        System.out.printf("Tipo de Cliente: %d\n", tipoCliente);
        System.out.printf("kWh Consumidos: %.2f\n", kWhConsumidos);
        System.out.printf("Valor Total: R$ %.2f\n", valorTotal);
    }
}
