package br.edu.up.Modelos;

public class Seguradora {
    public static String determinarCategoria(int idade, int grupoRisco) {
        if (idade >= 17 && idade <= 20) {
            return categoriaPorGrupoRisco(grupoRisco, 1, 2, 3);
        } else if (idade >= 21 && idade <= 24) {
            return categoriaPorGrupoRisco(grupoRisco, 2, 3, 4);
        } else if (idade >= 25 && idade <= 34) {
            return categoriaPorGrupoRisco(grupoRisco, 3, 4, 5);
        } else if (idade >= 35 && idade <= 64) {
            return categoriaPorGrupoRisco(grupoRisco, 4, 5, 6);
        } else if (idade >= 65 && idade <= 70) {
            return categoriaPorGrupoRisco(grupoRisco, 7, 8, 9);
        } else {
            return null;
        }
    }

    private static String categoriaPorGrupoRisco(int grupoRisco, int baixo, int medio, int alto) {
        switch (grupoRisco) {
            case 1:
                return String.valueOf(baixo);
            case 2:
                return String.valueOf(medio);
            case 3:
                return String.valueOf(alto);
            default:
                return null;
        }
    }
}