package br.edu.up.Modelos;

public class TipoTriangulo {

    public boolean ehTrianguloValido(int a, int b, int c) {
        return a + b > c && a + c > b && b + c > a;
    }

    public String tipoTriangulo(int a, int b, int c) {
        if (!ehTrianguloValido(a, b, c)) {
            return "Não forma um triângulo";
        }

        if (a == b && b == c) {
            return "Triângulo Equilátero";
        } else if (a == b || a == c || b == c) {
            return "Triângulo Isóscele";
        } else {
            return "Triângulo Escaleno";
        }
    }
}