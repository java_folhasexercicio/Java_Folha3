package br.edu.up.Modelos;

public class Funcionario {
    private String nome;
    private int idade;
    private char sexo;
    private double salario;
  

    public Funcionario(String nome, int idade, char sexo, double salario) {
      this.nome = nome;
      this.idade = idade;
      this.sexo = sexo;
      this.salario = salario;
    }

    public double CalcularSalarioLiquido() {
      return salario + calcularAbono();
    }

    private double calcularAbono() {
      double valorAbono = 0;
  
      if (idade >= 21) {
        valorAbono += 50;
      }
      if (idade >= 30) {
        valorAbono += 100;
      }
  
      if (sexo == 'M') {
        valorAbono += 25;
      } else if (sexo == 'F') {
        valorAbono += 35;
      } else {
        System.out.println("Sexo inválido. Considerado 'M' para cálculo.");
      }
  
      return valorAbono;
    }
  
    public void imprimirDetalhes() {
      System.out.println("\nFuncionário(a): " + nome);
      System.out.printf("Salário Líquido: R$ %.2f\n", CalcularSalarioLiquido());
    }
  }
