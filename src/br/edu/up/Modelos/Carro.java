package br.edu.up.Modelos;

public class Carro {
    
    public double Valor;
    public int Ano;

    public double CalculaValorFinal()
    {
        if(this.Ano <= 2000)
        {
           double desconto =  this.Valor * 0.12;
           return this.Valor - desconto;
        }
        else
        {
            double desconto =  this.Valor * 0.7;
            return this.Valor - desconto;
        }
    }
}
