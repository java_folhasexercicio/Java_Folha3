package br.edu.up.Modelos;

public class Carango {
    public double Valordesconto;
    public double ValorPago;

    public void CalculaCombustivel(String tipoCombustivel) {

        if (tipoCombustivel == "A") {

            this.Valordesconto = this.ValorPago * 0.25;
        } else if (tipoCombustivel == "G") {
            this.Valordesconto = this.ValorPago * 0.21;
        } else if (tipoCombustivel == "D") {
            this.Valordesconto = this.ValorPago * 0.14;
        }
    }
}
