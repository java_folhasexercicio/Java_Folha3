package br.edu.up.Modelos;

public class Salario {

    public double SalarioAtual;
    public double SalarioReajuste;

    public void CalcularReajuste()
    {
        if(this.SalarioAtual <= (1500 * 3))
        {
            this.SalarioReajuste = this.SalarioAtual + (this.SalarioAtual * 0.50);
        }
        else if(this.SalarioAtual <= (1500 * 10) && this.SalarioAtual >= (1500 * 3))
        {
            this.SalarioReajuste = this.SalarioAtual + (this.SalarioAtual * 0.20);
        }
        else if(this.SalarioAtual <= (1500 * 20) && this.SalarioAtual >= (1500 * 10))
        {
            this.SalarioReajuste = this.SalarioAtual + (this.SalarioAtual * 0.15);
        }
        else
        {
            this.SalarioReajuste = this.SalarioAtual + (this.SalarioAtual * 0.10);
        }
    }
}
