package br.edu.up.Modelos;

public class Precificacao {

    public double CustoDeFabrica;
    public double ValorFinal;

    public Precificacao(double custo) {

        this.CustoDeFabrica = custo;

        double ValorImpostos = this.CustoDeFabrica * 0.45;
        double PercentagemDistribuidor = (this.CustoDeFabrica + ValorImpostos) * 0.23;

        this.ValorFinal = this.CustoDeFabrica + ValorImpostos + PercentagemDistribuidor;
    }
}
