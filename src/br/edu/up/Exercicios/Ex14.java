package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex14 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        List<Produto> produtos = new ArrayList();
        double somaCusto = 0;
        double somaVenda = 0;

        for(int i = 0; i < 40; i++)
        {
            Produto produto = new Produto();
            produto.Descricao = "Produto" + i;

            if (i % 2 == 0) {
                produto.PrecoCusto = 23.70;
                produto.PrecoVenda = 20.35;

            } else {
                produto.PrecoCusto = 21.70;
                produto.PrecoVenda = 69.35;
            }

            produtos.add(produto);

            System.out.println(produto.Descricao);
            System.out.println(produto.CalculaStatus());

            somaCusto = somaCusto + produto.PrecoCusto;
            somaVenda = somaVenda + produto.PrecoVenda;
        }

        double mediaCusto = somaCusto / 40;
        double mediaVenda = somaVenda / 40;

        System.out.println("A media do preco de custo e: " + mediaCusto);
        System.out.println("A media do preco de venda e: " + mediaVenda);

        scanner.close();
    }
}

