package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex20 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o nome do professor: ");
        String nome = scanner.nextLine();

        System.out.print("Digite o nível do professor (1, 2 ou 3): ");
        int nivel = scanner.nextInt();

        System.out.print("Digite a quantidade de horas/aula trabalhadas: ");
        int horasAula = scanner.nextInt();

        Professor professor = new Professor(nome, nivel, horasAula);

        professor.imprimirResumoSalario();

        scanner.close();
    }
}

