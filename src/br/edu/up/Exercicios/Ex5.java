package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex5 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o valor total da compra: ");
        double valor = scanner.nextDouble();

        Compra compra = new Compra(valor);

        System.out.println("Em 5x, o valor da prestação fica: " + compra.ValorPrestacao);

        scanner.close();
    }
}
