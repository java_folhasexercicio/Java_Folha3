package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.Produto;

public class Ex6 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Fale o preco de custo :");
        double PrecoCusto = scanner.nextDouble();

        System.out.println("Fale o percentual de acréscimo (%):");
        double percentual = scanner.nextDouble();

        Produto produto = new Produto(PrecoCusto, percentual);

        double valorVenda = produto.calcularValorVenda();

        System.out.println("O valor de venda é: R$" + valorVenda);

        scanner.close();
    }

}
