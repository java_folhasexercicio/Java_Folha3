package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex26 {

    public void Executar() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o nome do pretendente:");
        String nome = scanner.nextLine();

        System.out.println("Digite a idade do pretendente:");
        int idade = scanner.nextInt();

        System.out.println("Digite o grupo de risco do pretendente (1 - Baixo, 2 - Médio, 3 - Alto):");
        int grupoRisco = scanner.nextInt();

        // Determinar e imprimir a categoria do seguro usando os métodos da classe
        // Seguradora
        String categoria = Seguradora.determinarCategoria(idade, grupoRisco);
        if (categoria != null) {
            System.out.println("Nome: " + nome);
            System.out.println("Categoria de seguro: " + categoria);
        } else {
            System.out.println("Desculpe, o pretendente não se enquadra em nenhuma das categorias ofertadas.");
        }

        scanner.close();
    }

}
