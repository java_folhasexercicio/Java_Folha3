package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex24 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        CalculadoraNota calculadoraNota = new CalculadoraNota();

        System.out.println("Digite a nota do trabalho de laboratório:");
        calculadoraNota.NotaLaboratorio = scanner.nextDouble();

        System.out.println("Digite a nota da avaliação semestral:");
        calculadoraNota.NotaAvSemestral = scanner.nextDouble();

        System.out.println("Digite a nota do exame final:");
        calculadoraNota.ExameFinal = scanner.nextDouble();

        var notaFinal = calculadoraNota.CalculaMedia();

        System.out.println("A nota final do aluno é: " + notaFinal);

        scanner.close();
    }
}
