package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex23 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        IMC imc = new IMC();

        System.out.print("Informe a altura da pessoa: ");
        imc.Altura = scanner.nextDouble();

        System.out.print("Informe a idade da pessoa: ");
        imc.Idade = scanner.nextInt();

        System.out.print("Informe o sexo da pessoa (M)Masculino | (F)Feminino: ");
        scanner.nextLine();
        var sexo = scanner.nextLine();

        if (sexo == "M") {
            imc.CalculaImcHomem();
        } else if (sexo == "F") {
            imc.CalculaImcMulher();
        }

        System.out.println("O peso ideal da pessoa: " + imc.PesoIdeal);

        scanner.close();
    }
}
