package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.Intervalo;

public class Ex9 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);
        int contador = 0;

        System.out.println("Digite 80 números:");

        for (int i = 0; i < 80; i++) {
            int numero = scanner.nextInt();
            Intervalo intervalo = new Intervalo(numero);
            
            if (intervalo.estaDentroDoIntervalo()) {
                contador++;
            }
        }

        System.out.println("Total de números no intervalo entre 10 e 150: " + contador);
        
        scanner.close();
    }
}
    

