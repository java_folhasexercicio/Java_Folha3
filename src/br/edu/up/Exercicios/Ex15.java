package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex15 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        double totalDesconto = 0;
        double totalPago = 0;

        Carango carango = new Carango();

        while (true) {
            System.out.print("Digite o valor do veículo (ou 0 para encerrar): ");
            carango.ValorPago = scanner.nextDouble();

            if (carango.ValorPago <= 0) {
                break;
            }

            System.out.print("Informe o tipo de combustível (Álcool/A, Gasolina/G, Diesel/D): ");
            scanner.nextLine();
            String tipoCombustivel = scanner.nextLine();

            carango.CalculaCombustivel(tipoCombustivel);

            System.out.println("Desconto: R$" + carango.Valordesconto);
            double totalPagar = (carango.ValorPago - carango.Valordesconto);
            System.out.println("Valor a ser pago: R$" + totalPagar);

            totalDesconto += carango.Valordesconto;
            totalPago += totalPagar;
        }

        System.out.println("Total de desconto concedido: R$ " + totalDesconto);
        System.out.println("Total pago pelos clientes: R$ " + totalPago);
        scanner.close();

    }

}
