package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex17 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        String nomeFuncionario = "João Silva";
        double salarioAtual = 1200.00;
        double salarioMinimo = 1300.00;

        ReajusteSalarial reajuste = new ReajusteSalarial(nomeFuncionario, salarioAtual, salarioMinimo);
        reajuste.imprimeResultados();

        scanner.close();
    }
}
