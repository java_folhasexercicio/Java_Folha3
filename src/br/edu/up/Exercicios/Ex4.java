package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex4 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        Conversao conversao = new Conversao();

        System.out.println("Informe a cotação do dolar hoje: ");
        conversao.CotacaoDolar = scanner.nextDouble();

        System.out.println("Informe o valor em dolar que deseja converter: ");
        conversao.ValorReal = scanner.nextDouble();

        var resultado = conversao.Converte();

        System.out.println("O valor convertido é: R$ " + resultado);

        scanner.close();
    }
}
