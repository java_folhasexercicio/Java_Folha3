package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;


public class Ex11 {

        public void Executar() {
        Scanner scanner = new Scanner(System.in);

        int ContadorHomens = 0;
        int ContadorMulheres = 0;
        List<Pessoa> pessoas = new ArrayList();

        for(int i = 0; i < 56; i++)
        {
            Pessoa pessoa = new Pessoa();

            pessoa.Nome = "Pessoa" + i;

            if (i % 2 == 0) {
                pessoa.Sexo = "M";

            } else {
                pessoa.Sexo = "F";
            }

            pessoas.add(pessoa);
        }

        for(var pessoa : pessoas)
        {
            if(pessoa.Sexo == "M")
            {
                ContadorHomens = ContadorHomens + 1;
            }
            else
            {
                ContadorMulheres = ContadorMulheres + 1;
            }
        }

        System.out.println("Total de homens: " + ContadorHomens);
        System.out.println("Total de homens: " + ContadorMulheres);


        scanner.close();
    }

}
