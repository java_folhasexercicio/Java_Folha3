package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex21 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        Nadador nadador = new Nadador();

        System.out.println("Informe a idade do nadador: ");
        nadador.Idade = scanner.nextInt();

        var nivel = nadador.classificarCategoria();

        System.out.println("O nível do nadador é: " + nivel);

        scanner.close();
    }
}
