package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex22 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o nome do cliente: ");
        String nomeCliente = scanner.nextLine();

        System.out.print("Digite o tipo de cliente (1, 2 ou 3): ");
        int tipoCliente = scanner.nextInt();

        System.out.print("Digite a quantidade de kWh consumidos: ");
        double kWhConsumidos = scanner.nextDouble();

        ContaLuz contaLuz = new ContaLuz(nomeCliente, tipoCliente, kWhConsumidos);

        contaLuz.imprimirResumo();

        scanner.close();
    }
}
