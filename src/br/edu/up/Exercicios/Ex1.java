package br.edu.up.Exercicios;

import java.util.Scanner;

import br.edu.up.Modelos.*;

public class Ex1 {

    public void Executar() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o nome do aluno: ");
        var nome = scanner.nextLine();

        System.out.println("Informe a primeira nota: ");
        double nota1 = scanner.nextDouble();

        System.out.println("Informe a segunda nota: ");
        double nota2 = scanner.nextDouble();

        System.out.println("Informe a terceira nota: ");
        double nota3 = scanner.nextDouble();

        Aluno aluno = new Aluno(nome, nota1, nota2, nota3);

        System.out.println("O aluno '" + aluno.Nome + "' teve média aluno.Media " + aluno.Media);

        scanner.close();
    }
}
