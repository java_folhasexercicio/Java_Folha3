package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex7 {

    public void Executar() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Informe o custo de fabricação do veículo: ");
        double custoFabricacao = scanner.nextDouble();

        Precificacao precificacao = new Precificacao(custoFabricacao);

        System.out.println("O valor final do veículo é: " + precificacao.ValorFinal);

        scanner.close();
    }
}
