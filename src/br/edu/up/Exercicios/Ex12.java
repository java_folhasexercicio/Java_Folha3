package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;


public class Ex12 {

        public void Executar() {
        Scanner scanner = new Scanner(System.in);

        List<Carro> carros = new ArrayList<>(); 
        String continua = "S";

        while (continua.equals("S")) {
            Carro carro = new Carro();

            System.out.println("Informe o ano do carro: ");
            carro.Ano = scanner.nextInt();

            System.out.println("Informe o valor do carro: ");
            carro.Valor = scanner.nextDouble();

            var valorFinal = carro.CalculaValorFinal();

            System.out.println("O valor com desconto do carro fica: " + valorFinal);
        
            carros.add(carro);

            System.out.println("Voce deseja continuar? (S)sim : (N)nao");
            scanner.nextLine();
            continua = scanner.nextLine();
        }

        int TotalCarros2000 = 0;
        int TotalGeral = 0;
        for(var carro : carros)
        {
            if(carro.Ano <= 2000)
            {
                TotalCarros2000 = TotalCarros2000 + 1;
            }

            TotalGeral = TotalGeral + 1;
        }
        
        System.out.println("O total de carros com ano ate 2000: " + TotalCarros2000);
        System.out.println("O total de carros em geral e : " + TotalGeral);

        scanner.close();
    }

}
