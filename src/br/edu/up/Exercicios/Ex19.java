package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex19 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        int a = 3;
        int b = 4;
        int c = 5;

        TipoTriangulo triangulo = new TipoTriangulo();

        System.out.println("Lados: " + a + ", " + b + ", " + c);

        if (triangulo.ehTrianguloValido(a, b, c)) {
            System.out.println("O triângulo formado é: " + triangulo.tipoTriangulo(a, b, c));
        } else {
            System.out.println("Os valores informados não formam um triângulo.");
        }

        scanner.close();
    }
}
