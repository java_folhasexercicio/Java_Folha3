package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.Vendedor;

public class Ex3 {

        public void Executar() {
            Scanner scanner = new Scanner(System.in);

    
            System.out.println("Digite o nome do vendedor:");
            String nome = scanner.nextLine();
    
            System.out.println("Digite o salário fixo do vendedor:");
            double salarioFixo = scanner.nextDouble();

    
            System.out.println("Digite o total de vendas feitas pelo vendedor(em dinheiro):");
            double totalVendas = scanner.nextDouble();
    
            Vendedor Vendedor = new Vendedor(nome, salarioFixo, totalVendas);
    
            double salarioFinal = Vendedor.calculaSalario();
    
            System.out.println("Nome do vendedor: " + Vendedor.Nome);
            System.out.println("Salário fixo: " + Vendedor.salarioFixo);
            System.out.println("Salário final do mês: " + salarioFinal);
    
            scanner.close();


    }
}