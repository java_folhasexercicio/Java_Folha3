package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex18 {

    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Digite o nome do funcionário: ");
        String nome = scanner.nextLine();
    
        System.out.print("Digite a idade do funcionário: ");
        int idade = scanner.nextInt();
    
        System.out.print("Digite o sexo do funcionário (M/F): ");
        char sexo = scanner.next().toUpperCase().charAt(0);
    
        System.out.print("Digite o salário fixo do funcionário: ");
        double salario = scanner.nextDouble();
    
        Funcionario funcionario = new Funcionario(nome, idade, sexo, salario);

        funcionario.imprimirDetalhes();

        scanner.close();
    }
}

