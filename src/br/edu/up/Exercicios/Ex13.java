package br.edu.up.Exercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import br.edu.up.Modelos.*;

import java.util.Scanner;

public class Ex13 {
    public void Executar() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Quantas pessoas serão avaliadas? ");
        int numPessoas = scanner.nextInt();
        scanner.nextLine();

        int Aptos = 0;
        int Inaptos = 0;

        for (int i = 0; i < numPessoas; i++) {
            MIlitar pessoa = new MIlitar();

            System.out.println("Dados da pessoa " + (i + 1) + ":");
            System.out.print("Nome: ");
            pessoa.Nome = scanner.nextLine();

            System.out.print("Sexo (M/F): ");
            pessoa.Sexo = scanner.nextLine();

            System.out.print("Idade: ");
            pessoa.Idade = scanner.nextInt();
            scanner.nextLine();

            System.out.print("Saúde (Apto/Inapto): ");
            pessoa.Saude = scanner.nextLine();

            if (pessoa.Sexo.equalsIgnoreCase("M") && pessoa.Idade >= 18 && pessoa.Saude.equalsIgnoreCase("Apto")) {
                Aptos++;
            } else {
                Inaptos++;
            }
        }

        System.out.println("\nTotal de pessoas aptas: " + Aptos);
        System.out.println("Total de pessoas inaptas: " + Inaptos);

        scanner.close();
    }
}
