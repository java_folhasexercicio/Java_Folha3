package br.edu.up.Exercicios;

import java.util.Scanner;
import br.edu.up.Modelos.*;

public class Ex2 {

    public void Executar() {

        Scanner scanner = new Scanner(System.in);

        Automovel automovel = new Automovel();

        System.out.println("Informe a distancia percorrida: ");
        automovel.DistanciaPercorrida = scanner.nextDouble();

        System.out.println("Informe a quantidade de combustível gasta: ");
        automovel.CombustivelGasto = scanner.nextDouble();

        var consumo = automovel.CalculaConsumo();

        System.out.println("O consumo do veículo é: " + consumo + "Km/L");

        scanner.close();

    }
}
